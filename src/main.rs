use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing::{info, instrument};
use tracing_subscriber::fmt::format::FmtSpan;

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct CustomEvent {
    n: i64,
}

#[derive(Serialize)]
struct CustomOutput {
    primes: Vec<i64>,
}

#[instrument]
async fn func(event: LambdaEvent<CustomEvent>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    info!("Received event: {:?}", event);
    let primes = find_primes_up_to(event.n);
    Ok(CustomOutput { primes })
}

fn find_primes_up_to(n: i64) -> Vec<i64> {
    let mut primes = Vec::new();
    for i in 2..=n {
        if is_prime(i) {
            primes.push(i);
        }
    }
    primes
}

fn is_prime(n: i64) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..=((n as f64).sqrt() as i64) {
        if n % i == 0 {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;
    use lambda_runtime::Context;

    #[tokio::test]
    async fn test_lambda_function() {
        let test_event = CustomEvent { n: 10 };
        let context = Context::default();

        let response = func(LambdaEvent::new(test_event, context)).await;
        assert!(response.is_ok());
        let output = response.unwrap();
        assert_eq!(output.primes, vec![2, 3, 5, 7]); // Prime numbers up to 10
    }
}
