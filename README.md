# Mini Project 6: Rust Lambda with Tracing and Logging

## Goals
- Add logging to a Rust Lambda function
- Integrate AWS X-Ray tracing
- Connect logs/traces to CloudWatch

## Project Description
This project enhances a Rust-based AWS Lambda function to calculate prime numbers up to a given integer. Leveraging AWS X-Ray and CloudWatch, the function is instrumented with advanced logging and tracing capabilities for in-depth monitoring and performance metrics.

## Function Description
The Lambda function accepts a single argument `n: integer`，and returns a list of all prime numbers up to and including `n`.

---

## Key Steps
1. Install [Cargo Lambda](https://www.cargo-lambda.info/guide/getting-started.html).
2. Create a new Lambda project and navigate into it:
```bash
cargo lambda new <Your Project Name> && cd <Your Project Name>
```
3. Implement your Lambda function in `main.rs`. Add logging and tracing by including `tracing` and `tracing-subscriber` dependencies in `Cargo.toml`.

4. Create a `.env` file to store your AWS credentials and region:
```plaintext
AWS_ACCESS_KEY_ID=your_access_key_id
AWS_SECRET_ACCESS_KEY=your_secret_access_key
AWS_REGION=your_aws_region
```
Add a `.gitignore` file to exclude environment files and build artifacts:
```plaintext
/target
.env
```
5. Load your AWS credentials into the environment. On Unix-based systems:
```bash
export $(cat .env | xargs)
```
Or, use `set -a; source .env; set +a` for a similar effect.

6. Ensure your function works locally by formatting, lint checking, and testing:
```bash
cargo fmt --quiet
cargo clippy --quiet
cargo test --quiet
```
7. Build your function for an AWS deployment:
```bash
cargo lambda build --release
```
8. Deploy your function to AWS:
```bash
cargo lambda deploy
```
9. In AWS IAM, create a user with `iamfullaccess`, `lambdafullaccess`, and `AmazonXrayFullAccess` policies. Under Secruity Credentials, generate a new `Access Key`.

10. In the AWS Lambda Console, confirm your function is deployed. Go to `Configuration` and enable Active tracing for `AWS X-Ray` and Enhanced monitoring for `CloudWatch Lambda Insights`.

11. Test your function using the Lambda runtime server and invocation command:
```bash
cargo lambda watch
# In a new terminal
cargo lambda invoke --data-ascii '{"n": 10}'
```
12. Go to `API Gateway`, create a `REST API` with the desired stage, and deploy it. Ensure your Lambda function is connected to the appropriate API Gateway resource and method.

13. Test the deployed API using `curl`:
```bash
curl -X POST https://csjhpuluc3.execute-api.us-east-1.amazonaws.com/newStage/mini-project6/mini6Resource\
  -H 'content-type: application/json' \
  -d '{ "n": 15 }'
```

14. After successful remote testing, check the `Monitor` section in the AWS Lambda Console for `Traces`, and navigate to `AWS CloudWatch` for detailed trace and log information.


## Deliverables
- Successful Invoke
![invoke0](screenshots/invoke.png)

![invoke1](screenshots/curl.png)

![invoke2](screenshots/Insomnia.png)

- CloudWatch and X-Ray
![enable](screenshots/EnableXRay.png)
![traces0](screenshots/CloudWatchTrace.png)

- Tracing details and logging info
![traces1](screenshots/XRayTraces.png)
![log](screenshots/LogEvents.png)